#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <time.h>
#include <regex>
#include <map>
#include <unordered_map>

using namespace std;

// 구분자별 문자열을 나눠주기 위한 메서드
vector<string> split(const string& s, char delimiter) {
    vector<string> tokens;
    size_t start = 0;
    size_t end = s.find(delimiter);
    while (end != string::npos) {
        string token = s.substr(start, end - start);
        if (!token.empty()) {
            tokens.push_back(token);
        }
        start = end + 1;
        end = s.find(delimiter, start);
    }
    string lastToken = s.substr(start, end);
    if (!lastToken.empty()) {
        tokens.push_back(lastToken);
    }
    return tokens;
}


// 아스키 코드값을 기준으로 영어,숫자를 제외한 문자를 공백으로 바꾸어 주는 메서드
string SpecialCharReplace(string input) {
    string output;
    for (char c : input) {
        if (!ispunct(c)) {
            output += c;
        }
        else {
            output += ' ';
        }
    }
    return output;
};

// key(단어)값에 대한 value값으로 사용하기 위한 구조체
struct Doc_freq {
    int doc_num;
    int freq;
};

// map에 대한 value값의 정렬을 위한 비교 메서드
bool compare(const Doc_freq& a, const Doc_freq& b) {
    if (a.freq == b.freq) {
        return a.doc_num < b.doc_num;
    }
    else {
        return a.freq > b.freq;
    }
}


int main() {

    map<string, vector<Doc_freq>> inv_value;

    std::ifstream file("input.big", std::ios::binary);
    std::vector<char> buffer(4096);
    clock_t start, end;
    double result;
    string f_text;
    string doc_num;

    start = clock();

    // 파일을 buffer 단위로 읽기
    if (file) {
        while (file) {
            file.read(buffer.data(), buffer.size());

            string contents(buffer.begin(), buffer.begin() + file.gcount());

            transform(contents.begin(), contents.end(), contents.begin(), ::tolower);

            f_text += contents;
        }
    }
    else {
        std::cerr << "파일을 열 수 없습니다." << std::endl;
    }

    f_text = SpecialCharReplace(f_text);

    //std::cout << f_text << endl;

    vector<string> line_text = split(f_text, '\n');

    unordered_map<string, int> word_freq;
    vector<string> word;

    for (auto iter = line_text.begin(); iter != line_text.end(); iter++) {
        word = split(*iter, ' ');

        doc_num = *word.begin();
        word.erase(word.begin());
        word_freq.clear();
        for (auto& i : word) {
            word_freq[i]++;
        }

        for (auto const& pair : word_freq) {
            inv_value[pair.first].push_back({ stoi(doc_num), pair.second });
        }

    }

    for (auto& val : inv_value) {
        sort(val.second.begin(), val.second.end(), compare);
    }

    cout << "총 문자 수 : " << inv_value.size() << endl;

    end = clock();
    result = (double)(end - start) / 1000;

    std::cout << "수행 시간 : " << result << "second" << "\n";



    // 역파일 생성을 위해 만든 데이터를 텍스트 파일로 만듬

    string out_text;

    std::ofstream outFile("output.big");
    for (const auto& var : inv_value) {
        out_text = var.first;
        for (const auto& df : var.second) {
            out_text += " " + to_string(df.doc_num) + " " + to_string(df.freq);
        }
        outFile << out_text << endl;
    }
    outFile.close();

    string input_word;



    // 단어를 입력 받고 단어에 대한 정보를 출력해줌
    while (true) {
        cout << "입력 : ";  getline(std::cin, input_word);;

        if (input_word == "종료") break;

        transform(input_word.begin(), input_word.end(), input_word.begin(), ::tolower);

        if (inv_value.find(input_word) != inv_value.end()) {
            for (auto const& pair : inv_value[input_word]) {
                cout << "[DOC] : " << pair.doc_num << "  [FREQ] : " << pair.freq << endl;
            }
        }
        else {
            cout << "찾을 수 없는 단어입니다." << endl;
        }
    }
}


